<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');
if (strlen($_SESSION['fosaid']==0)) {
  header('location:logout.php');
  } else{
}
  ?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Ganh Cambodia</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="css/plugins/steps/jquery.steps.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
            <!-- custom btn delete -->
            <style type="text/css">
                        a{
                            color:red;
                        }
                        a:hover{
                            color: red;
                        }
                    </style>
</head>

<body>

    <div id="wrapper">

    <?php include_once('includes/leftbar.php');?>

        <div id="page-wrapper" class="gray-bg">
             <?php include_once('includes/header.php');?>
      
        <div class="row border-bottom">
        
        </div>
            
        <div class="wrapper wrapper-content animated fadeInRight">
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        
                        <div class="ibox-content">
                                 <table class="table table-bordered mg-b-0">
                                     <!-- tab bar -->
                                     <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
        <a href="admin-create.php" class="btn btn-primary">Add</a>
           
        </div>
 <p style="font-size: 20px"><strong>Manage Admin User</strong></p>
        
            <ul class="nav navbar-top-links navbar-right"> </ul>

        </nav>
                                     <!-- end tab bar -->
                                   
              <thead>
                <tr>
                  <th>S.NO</th>
                  <th>Admin Name</th>
                  <th>UserName</th>
                  <th>Mobile Number</th>
                  <th>Email</th>
                  <th>Admin Registration date</th>
              
                   <th>Action</th>
                </tr>
              </thead>
              <?php
                $ret=mysqli_query($con,"select * from tbladmin");
                $cnt=1;
                // <!-- condition on delete role -->
                $admid=$_SESSION['fosaid'];
                $ret1=mysqli_query($con,"select UserName from tbladmin where ID='$admid'");
                $row=mysqli_fetch_array($ret1);
                $name=$row['UserName'];
                // end condition on delete role 
            while ($row=mysqli_fetch_array($ret)) {

                ?>
              <tbody>
                 <tr>
                  <td><?php  echo $cnt;?></td>
                  <td><?php  echo $row['AdminName'];?></td>
                  <td><?php $role_username = $row['UserName']; 
                    if($name == $role_username) {
                     echo "<span class='text-success'>".$role_username."</span>"; 
                    }else echo $role_username;
                    ?>
                  </td>
                  <td><?php  echo $row['MobileNumber'];?></td>
                  <td><?php  echo $row['Email'];?></td>
                  <td><?php  echo $row['AdminRegdate'];?></td>
                     <td>
                     <a style="margin-right: 15px; color: blue" href="admin-update.php?userid=<?php echo $row['ID'];?>">Edit</a>
            
                     <!-- condition on delete role -->
                     <?php 

                       if($name != $role_username) {
                           echo "<a onClick=\"javascript: return confirm('Are you sure to delete this?');\" 
                           href='admin-delete.php?userid=".$row['ID']."'>Delete</a>";
                       } elseif($name == $role_username) {
                            echo "<a style='margin-right: 15px; color: red' onClick=\"javascript: 
                            return alert('You cannot delete yourselt!');\">Delete</a>";
                        }
                        
                     ?>
                     <!-- end condition on delete role -->

                    </td>
                </tr>
                <?php 
                $cnt=$cnt+1;
                }?>

              </tbody>
            </table>
                           
                        </div>
                    </div>
                    </div>

                </div>
            </div>
         <?php include_once('includes/footer.php');?>

        </div>
        </div>



    <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>

    <!-- Steps -->
    <script src="js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="js/plugins/validate/jquery.validate.min.js"></script>


    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

</body>

</html>
