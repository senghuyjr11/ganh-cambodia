-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 24, 2021 at 08:53 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.0.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fos`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbladmin`
--

CREATE TABLE `tbladmin` (
  `ID` int(11) NOT NULL,
  `AdminName` varchar(45) DEFAULT NULL,
  `UserName` varchar(45) DEFAULT NULL,
  `MobileNumber` varchar(11) DEFAULT NULL,
  `Email` varchar(120) DEFAULT NULL,
  `Password` varchar(120) DEFAULT NULL,
  `AdminRegdate` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbladmin`
--

INSERT INTO `tbladmin` (`ID`, `AdminName`, `UserName`, `MobileNumber`, `Email`, `Password`, `AdminRegdate`) VALUES
(26, 'Senghuy', 'senghuy', '0966783671', 'senghuymit007@gmail.com', '202cb962ac59075b964b07152d234b70', '2021-10-24 06:48:00');

-- --------------------------------------------------------

--
-- Table structure for table `tblcategory`
--

CREATE TABLE `tblcategory` (
  `ID` int(5) NOT NULL,
  `CategoryName` varchar(120) DEFAULT NULL,
  `CreationDate` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblcategory`
--

INSERT INTO `tblcategory` (`ID`, `CategoryName`, `CreationDate`) VALUES
(11, 'Food', '2021-09-19 06:56:39'),
(12, 'Drink', '2021-09-19 06:56:57'),
(13, 'Dessert', '2021-09-19 06:57:24');

-- --------------------------------------------------------

--
-- Table structure for table `tblfood`
--

CREATE TABLE `tblfood` (
  `ID` int(10) NOT NULL,
  `CategoryName` varchar(120) DEFAULT NULL,
  `ItemName` varchar(120) DEFAULT NULL,
  `ItemPrice` varchar(120) DEFAULT NULL,
  `ItemDes` varchar(500) DEFAULT NULL,
  `Image` varchar(120) DEFAULT NULL,
  `ItemQty` varchar(120) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tblfood`
--

INSERT INTO `tblfood` (`ID`, `CategoryName`, `ItemName`, `ItemPrice`, `ItemDes`, `Image`, `ItemQty`) VALUES
(23, 'Food', 'ញាំត្រយូងចេកនិងបង្គា', '6.50', 'ម្ហូបប្រចាំហាង                                         	', 'd90cfded24e026e88600e603da406351.png', '1'),
(24, 'Food', 'ញាំសាច់គោជាមួយត្រកួន', '6.50', 'ម្ហូបថ្មីតែពេញនិយម                                                	', '6b8e20283141ec44cbb081ddeb392cc3.png', '1'),
(25, 'Food', 'ញាំគ្រឿងសមុទ្រពិសេស', '8', 'មុខម្ហូបលក់ដាច់ជាងគេ                                	', '9cce051b7b82b1ce91682fc02e5a715d.png', '1'),
(26, 'Food', 'ភ្លាត្រីពិសេស', '8', 'ម្ហូបខ្មែរ                                                 	', '35f59957c7998de6b7c6e80ac01c0d53.png', '1'),
(27, 'Food', 'ឈុតនំបញ្ចុកប្រហិតត្រី', '4.50', 'ម្ហូបវៀតណាម                                              	', '245320092d3888b1d66d2642a3ca8c97.png', '1'),
(28, 'Food', 'បាញ់ហយបង្គាអាំង', '6.50', 'មុខម្ហូបប្រចាំហាង                                                 	', '8e3c9c707dddbb9b93720fe924f7c83e.png', '1'),
(31, 'Drink', 'ទឹកផ្លែឈើស្រស់', '3', 'ចំរាញ់ចេញពីធម្មជាតិ                                                 	', '9ea068e71f150778fd5ca5cbd9d32189.png', '1'),
(32, 'Drink', 'ទឹកម្នាស់ក្រឡុក', '2.5', 'ជំនួយសុខភាព                                                 	', '472495aeaedcc97efb50ebfc7c4d0bbe.png', '1'),
(33, 'Dessert', 'ឈុតបង្អែម៣មុខ', '4', 'ការពិពណ៌នា                                                 	', '160b23bb19f821b3036777d7e86c8375.png', '1'),
(34, 'Dessert', 'បង្អែមគ្រាប់ឈូក', '3', 'ការពិពណ៌នា                                               	', 'bba97ea5f10d901502fc8f20da21add0.png', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tblfoodtracking`
--

CREATE TABLE `tblfoodtracking` (
  `ID` int(10) NOT NULL,
  `OrderId` char(50) DEFAULT NULL,
  `remark` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `status` char(50) DEFAULT NULL,
  `StatusDate` timestamp NULL DEFAULT current_timestamp(),
  `OrderCanclledByUser` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblfoodtracking`
--

INSERT INTO `tblfoodtracking` (`ID`, `OrderId`, `remark`, `status`, `StatusDate`, `OrderCanclledByUser`) VALUES
(12, '839827900', 'Order Confirmed', 'Order Confirmed', '2021-09-19 10:59:13', NULL),
(13, '839827900', 'Preparing', 'Food being Prepared', '2021-09-19 11:00:18', NULL),
(14, '839827900', 'Preparing', 'Food being Prepared', '2021-09-19 11:01:46', NULL),
(15, '394006228', 'Con', 'Order Confirmed', '2021-09-19 11:02:00', NULL),
(16, '466787365', 'fnfefe', 'Food being Prepared', '2021-10-18 07:55:57', NULL),
(17, '466787365', 'djjbvieee', 'Food Pickup', '2021-10-18 07:58:02', NULL),
(18, '466787365', 'djjbvieee', 'Food Pickup', '2021-10-18 08:00:11', NULL),
(19, '466787365', 'ដហេងេងេងងេង', 'Food Pickup', '2021-10-18 08:00:25', NULL),
(20, '921005159', 'ទទួលបាន', 'Order Confirmed', '2021-10-18 09:19:22', NULL),
(21, '265953346', 'its ok', 'Order Confirmed', '2021-10-18 10:50:52', NULL),
(22, '294037555', 'fegeh', 'Order Confirmed', '2021-10-18 10:54:35', NULL),
(23, '720178315', 'hello', 'Order Confirmed', '2021-10-18 11:03:21', NULL),
(24, '720178315', 'hi', 'Food being Prepared', '2021-10-18 11:04:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tblorderaddresses`
--

CREATE TABLE `tblorderaddresses` (
  `ID` int(11) NOT NULL,
  `UserId` char(100) DEFAULT NULL,
  `Ordernumber` char(100) DEFAULT NULL,
  `Flatnobuldngno` varchar(255) DEFAULT NULL,
  `StreetName` varchar(255) DEFAULT NULL,
  `Area` varchar(255) DEFAULT NULL,
  `Landmark` varchar(255) DEFAULT NULL,
  `City` varchar(255) DEFAULT NULL,
  `OrderTime` timestamp NOT NULL DEFAULT current_timestamp(),
  `OrderFinalStatus` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblorders`
--

CREATE TABLE `tblorders` (
  `ID` int(11) NOT NULL,
  `UserId` char(10) DEFAULT NULL,
  `FoodId` char(10) DEFAULT NULL,
  `IsOrderPlaced` int(11) DEFAULT NULL,
  `OrderNumber` char(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbluser`
--

CREATE TABLE `tbluser` (
  `ID` int(10) NOT NULL,
  `FirstName` varchar(45) DEFAULT NULL,
  `LastName` varchar(50) DEFAULT NULL,
  `Email` varchar(120) DEFAULT NULL,
  `MobileNumber` varchar(11) DEFAULT NULL,
  `Password` varchar(120) DEFAULT NULL,
  `RegDate` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbluser`
--

INSERT INTO `tbluser` (`ID`, `FirstName`, `LastName`, `Email`, `MobileNumber`, `Password`, `RegDate`) VALUES
(16, 'Senghuy', 'Chea', 'jsmith@sample.com', '0966783672', '202cb962ac59075b964b07152d234b70', '2021-10-24 06:28:48'),
(17, 'Senghuy', 'Chea', 'senghuymit007@gmail.com', '0966783671', '202cb962ac59075b964b07152d234b70', '2021-10-24 06:28:59');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbladmin`
--
ALTER TABLE `tbladmin`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblcategory`
--
ALTER TABLE `tblcategory`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblfood`
--
ALTER TABLE `tblfood`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblfoodtracking`
--
ALTER TABLE `tblfoodtracking`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblorderaddresses`
--
ALTER TABLE `tblorderaddresses`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `UserId` (`UserId`,`Ordernumber`);

--
-- Indexes for table `tblorders`
--
ALTER TABLE `tblorders`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `UserId` (`UserId`,`FoodId`,`OrderNumber`);

--
-- Indexes for table `tbluser`
--
ALTER TABLE `tbluser`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbladmin`
--
ALTER TABLE `tbladmin`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `tblcategory`
--
ALTER TABLE `tblcategory`
  MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tblfood`
--
ALTER TABLE `tblfood`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `tblfoodtracking`
--
ALTER TABLE `tblfoodtracking`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `tblorderaddresses`
--
ALTER TABLE `tblorderaddresses`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tblorders`
--
ALTER TABLE `tblorders`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `tbluser`
--
ALTER TABLE `tbluser`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
